---
title: Café multi-facettes 🪩 
subtitle: Retour sur l'ouverture de jeux de données pour <br> le ministère de l'Agriculture <br> (projet ma-cantine)
author: Quentin Loridant
date: 5 décembre 2023
title-slide-attributes:
    data-background-image: "static/logo.svg, static/logo_client.svg"
    data-background-size: "auto 10%, auto 12%"
    data-background-position: "95% 95%, 70% 97%"
---

# Contexte

## Le projet ma-cantine 

:::::::::::::: {.columns}
::: {.column width="60%"}
- Mission : **"Accompagner la transition alimentaire dans les assiettes et les filières agricoles"** 
- Format : Start-Up d'Etat
- Equipe : Dizaine de personnes
::: 

::: {.column width="40%"}
![](images/logo.jpg)
:::
::::::::::::::


---


## Historique 

- Arrivée en mai 2023 sur le projet
- Recruté en tant qu'ingénieur données

Mes missions sont principales sont:

* Soutien back-end sur le développement de l'application
* Analyse de données (bilan annuel, vizus...)
* Traitement de données (bilan annuel, export open-data...) 

# L'ouverture

## [Résultats des campagnes de Télédéclaration](https://www.data.gouv.fr/fr/datasets/resultats-de-campagnes-de-teledeclaration-des-cantines)

:::::: {.columns}
:::{.column .text-center .card .card width="50%"}
Un jeu de données administratif, important mais à l'impact restraint
:::
:::{.column .text-center .card .card-secondary width="50%"}
![](images/stats_td.png)
:::
::::::


## Ouverture colatérale : [le registre national des cantines](https://www.data.gouv.fr/fr/datasets/registre-national-des-cantines/#/information)

:::::: {.columns}
:::{.column .text-center .card .card width="40%"}
Une référénce, incomplète mais déjà plébiscitée
:::
:::{.column .text-center .card .card-secondary width="60%"}
![](images/stats_cantines.png)
:::
::::::

## Les réutilisations

* Le projet Cantines 1€ Egalim
* La fin des conventions chronophage pour les acteurs du milieu (DRAAF, Universitaires...)
* Vous ?

## Ressources utilisées

* Canal datagouv du mattermost Beta Gouv
* Forum team open data
* L'équipe multi

# Conclusion

## Pourquoi les données n'étaient pas encore ouvertes ?

Le contexte :

:::incremental
- Le projet existe depuis plusieurs années. 
- Deux profils full-stack expérimentés  
- Culture de l'équipe en faveur
- Soutien de juristes pro-actifs

- **Nécessite un profil dédié ?**
:::

## Et maintenant ?

- **Communication** : Faire connaître le jeu de données
- **Qualité** : Augmenter la représentativité des cantines (aujourd'hui un quart environ des cantines sont référencées)
- **Technique** : Gérer les données sous forme de listes 


## Conclusion personnelle

* Très content de découvrir le côté "producteur de données"
* Hâte de voir le nombre de cantines augmenter
* Mise en avant de la nécessité de ressources importantes pour ce type d'ouverture
* Mise en avant des **gains ce type d'ouverture**

# Des questions ?


